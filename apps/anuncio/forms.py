# -*- coding: utf-8 -*-
from django.forms import ModelForm
from models import *


class AnuncioForm(ModelForm):
    class Meta:
        model = Anuncio
        fields = []

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True
