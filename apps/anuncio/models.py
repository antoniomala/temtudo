# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from django.forms import ModelForm
from django.contrib.auth.models import User


class Anuncio(models.Model):
    usuario = models.ForeignKey(User, null=True, blank=True)
    titulo = models.CharField(u'', max_length=50)
    telefone1 = models.PhoneNumberField(u'')
    telefone2 = models.PhoneNumberField(u'')
    categoria = models.CharField(u'', max_length=50)
    bairro = models.CharField(u'', max_length=50)
    descricao = models.TextField(u'')
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    def save(self, *args, **kwargs):
        super(Anuncio, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Anúncio'
        verbose_name_plural = u'Anúncios'

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


class Promocoes(models.Model):
    usuario = models.ForeignKey(User, null=True, blank=True)
    titulo = models.CharField(u'', max_length=50)
    categoria = models.CharField(u'', max_length=50)
    bairro = models.CharField(u'', max_length=50)
    descricao = models.TextField(u'')
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    def save(self, *args, **kwargs):
        super(Anuncio, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Promoção'
        verbose_name_plural = u'Promoções'

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


class Novidade(models.Model):
    usuario = models.ForeignKey(User, null=True, blank=True)
    titulo = models.CharField(u'', max_length=50)
    categoria = models.CharField(u'', max_length=50)
    bairro = models.CharField(u'', max_length=50)
    descricao = models.TextField(u'')
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    def save(self, *args, **kwargs):
        super(Anuncio, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Novidade'
        verbose_name_plural = u'Novidades'

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


class Dica(models.Model):
    usuario = models.ForeignKey(User, null=True, blank=True)
    titulo = models.CharField(u'', max_length=50)
    categoria = models.CharField(u'', max_length=50)
    bairro = models.CharField(u'', max_length=50)
    descricao = models.TextField(u'')
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    def save(self, *args, **kwargs):
        super(Anuncio, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Dica'
        verbose_name_plural = u'Dicas'

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


class FaleConosco(object):
    email = models.EmailField(u'')
    nome = models.CharField(u'', max_length=50)
    telefone = models.PhoneNumberField(u'')
    mensagem = models.TextField(u'')
