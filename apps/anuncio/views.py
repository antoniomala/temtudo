# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import *


@login_required(login_url='/entrar/')
def cadastrar_anuncio(request):
    form = AnuncioForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form = form.save(commit=False)
        form.usuario = request.user
        form.save()
        return HttpResponseRedirect('/home')

    return render(request, 'anuncio_novo.html', {'form': form})


@login_required(login_url='/entrar/')
def editar_anuncio(request, id):
    instancia = Anuncio.objects.get(pk=id)
    form = AnuncioForm(request.POST or None, request.FILES or None, instance=instancia)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/home')

    return render(request, 'editar_anuncio.html', {'form': form, 'instancia': instancia})
