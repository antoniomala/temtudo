# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm


class EntrarForm(AuthenticationForm):
    class Meta:
        model = User
        exclude = []

    def __init__(self, *args, **kwargs):
        super(EntrarForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'usuario ou email'})

        self.fields['password'].widget = forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': 'Digite sua senha'})
